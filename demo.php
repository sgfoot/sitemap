<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/17
 * Time: 14:53
 */
require 'SgSiteMap.php';
$act = isset($_REQUEST['act']) ? trim($_REQUEST['act']) : 'index';

$obj = new demo();
if(!method_exists($obj, $act)) {
    exit($act . '方法不存在');
}
call_user_func(array($obj, $act));

class demo
{
    /**
     * 简单使用
     */
    public function index()
    {
        $obj = new SgSiteMap();
        $obj->add('/', 1, 3);
        $obj->finish();
    }
    public function init()
    {
        $obj = new SgSiteMap('http://sitemap.sgfoot.com');
        $obj->add('sitemap.xml');
        $obj->finish();
    }

    /**
     *
     */
    public function path()
    {
        $obj = new SgSiteMap('http://sitemap.sgfoot.com', __DIR__ . '/xml');
        $obj->add('sitemap.xml');
        $obj->finish();
    }

    /**
     * 设置时区
     */
    public function zone()
    {
        //第一种方式
        $obj = new SgSiteMap('http://sitemap.sgfoot.com', __DIR__ . '/xml', 'PRC');
        $obj->add('sitemap.xml');
        $obj->finish();
        //第二种方式
        $obj = new SgSiteMap('http://sitemap.sgfoot.com', __DIR__ . '/xml');
        $obj->setTimeZone('Asia/Shanghai');
        $obj->add('sitemap.xml');
        $obj->finish();
    }

    /**
     * xml 转换成 html
     */
    public function html()
    {
        //普通写法
        $obj = new SgSiteMap('http://sitemap.sgfoot.com', __DIR__ . '/xml', 'PRC');
        $obj->add('sitemap.xml');
        $obj->finish();
        $obj->toHtml();
        //连写
        $obj = new SgSiteMap('http://sitemap.sgfoot.com', __DIR__ . '/xml', 'PRC');
        $obj->add('sitemap.xml')->add('index.php')->finish()->toHtml();
    }

    /**
     * 连写
     */
    public function join()
    {
        $obj = new SgSiteMap();
        $obj->add('a')->add('b')->add('c')->finish()->toHtml();
    }

    /**
     * 设置是否强制替换host
     */
    public function force()
    {
        //如果setForce的话,add里的域名将会替换成初使化的host
        $obj = new SgSiteMap('http://wiki.sgfoot.com', __DIR__ . '/wiki');
        $obj->setForce();
        $obj->add('http://sitemap.sgfoot.com/home');
        $obj->add('project/follow');
        $obj->finish();
    }

    /**
     * 设置不同频率
     * 内置1~7个数值,也支持直接写:'always','hourly','daily','weekly','monthly','yearly','never',
     */
    public function freq()
    {
        $obj = new SgSiteMap();
        $obj->add('a', 1, 1);//第三个参数为设置更新频率
        $obj->finish();
    }

    /**
     * 设置链接不同权重 0~1,默认0.5
     */
    public function priority()
    {
        $obj = new SgSiteMap();
        $obj->add('a', 0.7);//第二个参数为设置链接不同权重
        $obj->finish();
    }
}