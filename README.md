# sitemap

## 1.介绍
* [sitemap.sgfoot.com](http://sitemap.sgfoot.com)
* 时光站点地图:一款生成站点地图,sitemap的工具
* 支持一次生成多个sitemap,sitemap最多支持5万条或10M以内,若超出则生成第二个sitemap文件
* 支持xml转换成html sitemap
* 支持线下生成线上的 sitemap.xml
* 频率设置1~7('always','hourly','daily','weekly','monthly','yearly','never')
* 链接权重0~1,默认为0.5

## 2.要求
* php5.1
* 开启:win: php_xsl.dll,linux:sudo apt-get install php5-xsl
* 应用到的核心函数:XMLWriter,XSLTProcessor,DOMDocument

## 3.使用说明
 ### 1.简单生成sitemap
 ```php
require 'SgSiteMap.php';
$obj = new SgSiteMap();
$obj->add('/', 1, 3);
$obj->add('/b.php', 1, 3);
$obj->finish();
 ```
 
 ### 2.初使化域名生成sitemap
 ```php
 $obj = new SgSiteMap('http://sitemap.sgfoot.com');
 $obj->add('sitemap.xml');
 $obj->finish();
 ```
 
### 3.初使化指定目录生成sitemap
```php
$obj = new SgSiteMap('http://sitemap.sgfoot.com', __DIR__ . '/xml');
$obj->add('sitemap.xml');
$obj->finish();
```

### 4.初使化设定时区生成sitemap
```php
//第一种方式
$obj = new SgSiteMap('http://sitemap.sgfoot.com', __DIR__ . '/xml', 'PRC');
$obj->add('sitemap.xml');
$obj->finish();
//第二种方式
$obj = new SgSiteMap('http://sitemap.sgfoot.com', __DIR__ . '/xml');
$obj->setTimeZone('Asia/Shanghai');
$obj->add('sitemap.xml');
$obj->finish();
```

### 5.xml转换成html
```php
//普通写法
$obj = new SgSiteMap('http://sitemap.sgfoot.com', __DIR__ . '/xml', 'PRC');
$obj->add('sitemap.xml');
$obj->finish();
$obj->toHtml();
//连写
$obj = new SgSiteMap('http://sitemap.sgfoot.com', __DIR__ . '/xml', 'PRC');
$obj->add('sitemap.xml')->add('index.php')->finish()->toHtml();
```
### 6.支持连写
```php
$obj = new SgSiteMap();
$obj->add('a')->add('b')->add('c')->finish()->toHtml();
```

### 7.设置是否强制替换host,应用于线下生成线上的sitemap
```php
//如果setForce的话,add里的域名将会替换成初使化的host
$obj = new SgSiteMap('http://wiki.sgfoot.com', __DIR__ . '/wiki');
$obj->setForce();
$obj->add('http://sitemap.sgfoot.com/home');
$obj->add('project/follow');
$obj->finish();
```

### 8.设置不同更新频率
```php
//内置1~7个数值,也支持直接写:'always','hourly','daily','weekly','monthly','yearly','never',
$obj = new SgSiteMap();
$obj->add('a', 1, 1);//第三个参数为设置更新频率
$obj->finish();
```

### 9.设置链接权重
```php
//设置链接不同权重 0~1,默认0.5
$obj = new SgSiteMap();
$obj->add('a', 0.7);//第二个参数为设置链接不同权重
$obj->finish();
```