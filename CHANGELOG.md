# CHANGE LOG

## v0.0.5 - 2017-11-17 PM
* 新增生成html的sitemap页面a标签 target="_blank"
   
## v0.0.4 - 2017-11-17 PM
* 链接不同权重 0~1,默认0.5 参考demo?act=priority

## v0.0.3 - 2017-11-17 PM
* 支持便捷设置更新频率1~7 参考demo?act=freq

## v0.0.2 - 2017-11-17 PM
* 新增支持线下生成线上的sitemap.xml 参考demo?act=force

## v0.0.1 - 2017-11-17 PM
* 上线生成sitemap项目